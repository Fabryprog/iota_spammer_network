import time
import json
import iota
import requests
import threading
import socket


"Initialization: second step"
serverURL = 'http://my-iota-node.com:5500'
"serverURL = 'http://localhost:5500'"
rx_address = 'IOTA9SPAMMER9NETWORK9BY9ITALY9999999999999999999999999999999999999999999999999999'
nodeURL = 'https://field.deviota.com:443'
hostName = socket.gethostname()
iteration = 0

"Function to post data to server"
def getDataFromServer(serverURL):
    global nodeURL
    global spammerIsActive
    try:
        getDataFromServerRequest = requests.get(url=serverURL)
        if getDataFromServerRequest.status_code == 200:
            responseJson = json.loads(getDataFromServerRequest.text)
            nodeURL = responseJson['nodeToSpam']
            spammerIsActive = responseJson['spammerIsActive']
        else:
            print(sendDataToServerRequest.text)
    except Exception as e:
        print(e)

"Function to generate a transaction"
def promoteTX(rx_address):
    global nodeURL
    api = iota.Iota(nodeURL)

    ### TX CREATION ##
    print("\n\n     ### TX CREATION ##")
    tx_message = "IOTA ITALIA RULEZ by " + hostName
    tx_tag = iota.Tag(b'IOTA9FULL9NODE9ITA')
    tx = iota.ProposedTransaction(address = iota.Address(rx_address),
                                  message = iota.TryteString.from_unicode(tx_message),
                                  tag = tx_tag,
                                  value = 0)
    print('     Created first transaction: ')
    print("     " + str(vars(tx)))

    ### BUNDLE FINALIZATION ###
    print("\n\n     ### BUNDLE FINALIZATION ###")
    bundle = iota.ProposedBundle(transactions = [tx])
    bundle.finalize()
    print("     Bundle is finalized...")
    print("     Generated bundle hash: %s" % (bundle.hash))
    print("     List of all transaction in the Bundle:\n")
    for txn in bundle:
        print("     " + str(vars(txn)))
    bundle_trytes = bundle.as_tryte_strings() # bundle as trytes

    ### TIP SELECTION ###
    print("\n\n     ### TIP SELECTION ###")
    tips = api.get_transactions_to_approve(depth = 3)
    print("     " + str(tips))

    ### POW ###
    print("\n\n     ### POW ###")
    try:
        attached_tx = api.attach_to_tangle(trunk_transaction=tips['trunkTransaction'], branch_transaction=tips['branchTransaction'], trytes=bundle_trytes, min_weight_magnitude=14)
    except:
        print('Can\'t execute POW and attach the TX to Tangle')

    ### BROADCASTING ###
    print("     Broadcasting transaction...")
    res = api.broadcast_and_store(attached_tx['trytes'])
    print("     " + str(res))

    ### TRANSACTION RECAP ###
    print("\n\n     ### TRANSACTION RECAP ###")
    print("     " + str(vars(attached_tx['trytes'][0])))

    sent_tx = iota.Transaction.from_tryte_string(attached_tx['trytes'][0])
    print("     Transaction Hash: " + str(sent_tx.hash))
    return ("")

"App initialization, including safety rule which toggle spammerIsActive after 60 minutes (in case server is not reachable)"
def initializationService():
    global spammerIsActive
    while True:
        spammerIsActive = 0
        time.sleep(3600)

"Check server data"
def checkServerDataService():
    while True:
        getDataFromServer(serverURL)
        time.sleep(1)

"Generate transaction if the bot is active"
def generateTransactionService():
    while True:
        if (spammerIsActive == 1):
            try:
                tx_hash_to_be_promoted = promoteTX(rx_address)
                iteration = iteration + 1
            except:
                print('Can\'t generate transaction')

"Threading initialization"
initializationService = threading.Thread(name='initialization', target=initializationService)
checkServerDataService = threading.Thread(name='getDataFromServer', target=checkServerDataService)
generateTransactionService = threading.Thread(name='generateTransactionService', target=generateTransactionService)

"App execution"
initializationService.start()
checkServerDataService.start()
generateTransactionService.start()