import telepot
from telepot.loop import MessageLoop
import time
import json
import iota
import requests
import datetime
import jwt
import datetime
import sys
import os


"Initialize configFile path"
base_dir = sys.path[0]
configFileName = 'config.txt'
full_path = os.path.join(base_dir, configFileName)

"Read configuration info from file: Bot token, node_name and password"
while True:
    try:
        with open(full_path) as configFile:
            configfileData = json.load(configFile)
        print('Opening file...')
        botToken = configfileData['bot_token']
        admin_list = configfileData['admin_list']
        secret_word = configfileData['secret_word']
        nodeURL = configfileData['node_to_spam']
        print("Initialization completed!")
        break
    except:
        print("'config.txt' file not found...\n")
        input("After file creation, press enter to retry...\n\n");

"Generates the Auth Token and return: string"
def encode_auth_token(secret_word):
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=999999),
            'iat': datetime.datetime.utcnow(),
            'sub': secret_word
        }
        return jwt.encode(
            payload,
            secret_word,
            algorithm='HS256'
        )
    except Exception as e:
        return e

"Initialization"
myBot = telepot.Bot(botToken)
serverURL = 'http://localhost:5500'
secret_token = encode_auth_token(secret_word).decode('utf-8')


"Define the message listener function"
def messageListener(msg):
    global nodeURL
    content_type, chat_type, chat_id = telepot.glance(msg)
    user_name = msg['from']['username']
    user_id = msg['chat']['id']
    if (content_type == 'text') & (any(user_name in x for x in admin_list)):
        if (msg['text'] == '/start') or (msg['text'] == '/start@IotaNetworkBot'):
            myBot.sendMessage(user_id, 'Ready to SPAM!')
        if (msg['text'] == '/check_spammer_network') or (msg['text'] == '/check_spammer_network@IotaNetworkBot'):
            spammerNetworkReport = checkSpammerNetwork()
            print(spammerNetworkReport)
            myBot.sendMessage(user_id, str(spammerNetworkReport))
        if ('/target_node' in msg['text']):
            try:
                nodeURL = msg['text'].split('|')[1]
                myBot.sendMessage(user_id, 'Target node has been changed to ' + nodeURL + '\n' + 'Send /spam command to load the new target node')
            except Exception as e:
                myBot.sendMessage(user_id, 'Wrong command: ' + str(e))
        if (msg['text'] == '/stop') or (msg['text'] == '/stop@IotaNetworkBot'):
            result, response = postDataToServer(nodeURL, 0)
            print('Sending /STOP command to server')
            if result == 200:
                myBot.sendMessage(user_id, 'Access: ' + response)
                myBot.sendMessage(user_id, 'STOP SPAMMING: ' + nodeURL)
            else:
                myBot.sendMessage(user_id, 'Can\'t send send data to server')
        if (msg['text'] == '/spam') or (msg['text'] == '/spam@IotaNetworkBot'):
            result, response = postDataToServer(nodeURL, 1)
            print('Sending /SPAM command to server')
            if result == 200:
                myBot.sendMessage(user_id, 'Access: ' + response)
                myBot.sendMessage(user_id, 'START SPAMMING: ' + nodeURL)
            else:
                myBot.sendMessage(user_id, 'Can\'t send send data to server')
            time.sleep(1)


"Function to post data to server"
def postDataToServer(nodeToSpam, spammerIsActive):
    parameters = {
                'nodeToSpam': nodeToSpam,
                'spammerIsActive': spammerIsActive,
                'secretToken': secret_token
                }
    headers = {'Content-Type': 'application/json'}
    "Send nodeURL and spammerIsActive data to server"
    try:
        sendDataToServerRequest = requests.post(url=serverURL, data=json.dumps(parameters), headers=headers)
        if sendDataToServerRequest.status_code == 200:
            responseJson = json.loads(sendDataToServerRequest.text)
            if (responseJson['hasPermission'] == True):
                return sendDataToServerRequest.status_code, 'GRANTED'
            else:
                return sendDataToServerRequest.status_code, 'DENIED'
        else:
            print(sendDataToServerRequest.text)
            return 'NOT WORKING', 'Error'
    except Exception as e:
        print(e)
        return 'NOT WORKING', 'Error'

"Read active client list"
def checkSpammerNetwork():
    try:
        getActiveClientListFromServerRequest = requests.get(url=(serverURL+'/getactiveclientlist'))
        getServerDataRequest = requests.get(url=(serverURL))
        if (getActiveClientListFromServerRequest.status_code == 200) & (getServerDataRequest.status_code == 200):
            serverDataJson = json.loads(getServerDataRequest.text)
            serverDataNodeURL = serverDataJson['nodeToSpam']
            print(serverDataNodeURL)
            messageToSend = ('NETWORK STATUS' + '\n')
            messageToSend += ('\n')
            messageToSend += ('Target node: ' + serverDataNodeURL + '\n')
            messageToSend += ('\n')
            serverDataSpammerIsActive = serverDataJson['spammerIsActive']
            messageToSend += ('Spamming is active [1=YES 0=NO]: ' + str(serverDataSpammerIsActive) + '\n')
            messageToSend += ('\n')
            activeClientListJson = json.loads(getActiveClientListFromServerRequest.text)
            messageToSend += ('Active clients: ' + '\n')
            for client in activeClientListJson:
                messageToSend += (client + '\n')
            return messageToSend
        else:
            return 'Connection to server is not working'
    except Exception as e:
        return e


"Activate listening loop"
MessageLoop(myBot, messageListener).run_as_thread()
print('Listening ...')

"Keep the program running."
while 1:
    time.sleep(1)