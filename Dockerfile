FROM ubuntu:16.04

WORKDIR /opt/spammer

COPY IotaSpammerClient.py /opt/spammer/IotaSpammerClient.py

COPY Requirements.txt /opt/spammer/

RUN apt-get update; apt-get -y upgrade; apt-get install -y python3-dev python3-pip make gcc libssl-dev

RUN pip3 install -r /opt/spammer/Requirements.txt

CMD ["python3", "/opt/spammer/IotaSpammerClient.py"]

