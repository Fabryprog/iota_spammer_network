from flask import Flask, jsonify
import flask
import json
import jwt
import sys
import os
import threading
import time

"Initialize configFile path"
base_dir = sys.path[0]
configFileName = 'config.txt'
full_path = os.path.join(base_dir, configFileName)
clientAddressList = []

"Read configuration info from file: Bot token, node_name and password"
while True:
    try:
        with open(full_path) as configFile:
            configfileData = json.load(configFile)
        print('Opening file...')
        secret_word = configfileData['secret_word']
        print("Initialization completed!")
        break
    except:
        print("'config.txt' file not found...\n")
        input("After file creation, press enter to retry...\n\n");

"Initialization"
nodeToSpam = "NodeNameNotUpdated"
spammerIsActive = 0

"Starting server"
webServer = Flask(__name__)

"GET and POST management"
@webServer.route("/", methods=['GET', 'POST'])
def postInfo():
    global nodeToSpam
    global spammerIsActive
    global secret_word
    global clientAddressList
    if flask.request.method == 'POST':
        receivedData = flask.request.get_json()
        receivedSecretToken = receivedData['secretToken']
        tokenIsVerified = verifyReceivedToken(receivedSecretToken, secret_word)
        hasPermission = False
        if tokenIsVerified == True:
            nodeToSpam = receivedData['nodeToSpam']
            spammerIsActive = receivedData['spammerIsActive']
            hasPermission = True
        return jsonify({
            'hasPermission': hasPermission,
            'nodeToSpam': nodeToSpam,
            'spammerIsActive': spammerIsActive
        })
    else:
        clientAddress = flask.request.remote_addr
        if (any(clientAddress in x for x in clientAddressList) == False):
            clientAddressList.append(clientAddress)
        print(clientAddressList)
        return jsonify({
            'nodeToSpam': nodeToSpam,
            'spammerIsActive': spammerIsActive
        })

@webServer.route("/getactiveclientlist", methods=['GET'])
def getActiveClientList():
    global clientAddressList
    if flask.request.method == 'GET':
        return jsonify(clientAddressList)

"Authorization: Verification of received token"
def verifyReceivedToken(receivedSecretToken, secret_word):
    decoded_secret_word = ''
    try:
        decoded_secret_word = decode_auth_token(receivedSecretToken, secret_word)
    except Exception as e:
        print(e)
        return e
    if decoded_secret_word == secret_word:
        return True

"Decode the"
def decode_auth_token(secret_token, secret_word):
    try:
        decodedJson = jwt.decode(secret_token, secret_word, algorithms=['HS256'])
        return decodedJson['sub']
    except Exception as e:
        return e

"Reset clientAddressList after X seconds"
def resetClientAddressListService():
    global clientAddressList
    while 1:
        clientAddressList.clear()
        time.sleep(60)

"Activate the server"
def activateServerService():
    webServer.run(host='0.0.0.0', port=5500, debug=False)

"Threading initialization"
resetClientAddressListService = threading.Thread(name='resetClientAddressListService', target=resetClientAddressListService)
activateServerService = threading.Thread(name='activateServerService', target=activateServerService)

"Start application"
resetClientAddressListService.start()
activateServerService.start()