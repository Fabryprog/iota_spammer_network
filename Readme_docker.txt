[Build Image]

docker build -t iota-spammer .

[Docker Run]

docker run --name iota-spammer -v $PWD/config.txt:/opt/spammer/config.txt -d iota-spammer

[Docker show logs]

docker logs -f iota-spammer

[Docker stop and remove]

docker rm -f iota-spammer 
